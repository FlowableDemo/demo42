package com.study.demo;

import java.util.List;
import java.util.Map;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;
import org.flowable.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.flowable.engine.impl.util.CommandContextUtil;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;

public class ReceiveTaskExecutionListener implements ExecutionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void notify(DelegateExecution execution) {
		Map<String, Object> var = execution.getVariables();
		//启动下一个独立流程
		String nextDefinitionKey = var.get("nextDefinitionKey").toString();
		var.remove("nextDefinitionKey");
		
		//查询当前执行实例
		String currentExecutionId = execution.getId();
		var.put("needTriggerReceiveExecutionId", currentExecutionId);
		
		
		ProcessEngineConfigurationImpl processEngineConfigurationImpl = CommandContextUtil.getProcessEngineConfiguration();
		RuntimeService runtimeService = processEngineConfigurationImpl.getRuntimeService();
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(nextDefinitionKey,var);

		System.out.println("开始下一个流程，流程实例id: " + processInstance.getId());
		System.out.println("开始下一个流程，流程实例名称: " + processInstance.getProcessDefinitionName());
		
	}

}
