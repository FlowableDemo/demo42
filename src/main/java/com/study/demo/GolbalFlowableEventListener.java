package com.study.demo;

import org.flowable.common.engine.api.delegate.event.FlowableEngineEventType;
import org.flowable.common.engine.api.delegate.event.FlowableEvent;
import org.flowable.common.engine.api.delegate.event.FlowableEventListener;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.event.impl.FlowableEntityEventImpl;
import org.flowable.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.flowable.engine.impl.util.CommandContextUtil;

public class GolbalFlowableEventListener implements FlowableEventListener {

	public void onEvent(FlowableEvent event) {
		FlowableEngineEventType eventType = (org.flowable.common.engine.api.delegate.event.FlowableEngineEventType) event.getType();
		switch (eventType) {
		case PROCESS_COMPLETED:
			
			FlowableEntityEventImpl flowableEntityEventImpl = (FlowableEntityEventImpl) event;
			
			//获取执行实例id
			String executionId = flowableEntityEventImpl.getExecutionId();
			
			//获取配置类
			ProcessEngineConfigurationImpl processEngineConfigurationImpl = CommandContextUtil.getProcessEngineConfiguration();
			
			//获取运行类
			RuntimeService runtimeService =processEngineConfigurationImpl.getRuntimeService();
			
			//获取接受任务的执行实例id
			Object  needTriggerReceiveExecutionId = runtimeService.getVariable(executionId, "needTriggerReceiveExecutionId");
			
			if(needTriggerReceiveExecutionId !=null ) {
				//触发接受任务继续执行
				runtimeService.trigger(needTriggerReceiveExecutionId.toString());
			}
			break;

		default:
			break;
		}
	}

	public boolean isFailOnException() {
		return false;
	}

	public boolean isFireOnTransactionLifecycleEvent() {
		return false;
	}

	public String getOnTransaction() {
		return null;
	}

}
