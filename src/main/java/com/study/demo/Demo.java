package com.study.demo;

import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.DynamicBpmnService;
import org.flowable.engine.FormService;
import org.flowable.engine.HistoryService;
import org.flowable.engine.IdentityService;
import org.flowable.engine.ManagementService;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.ProcessEngines;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:flowable-context.xml")
public class Demo{

	private ProcessEngine processEngine;
	private TaskService taskService;
	private RuntimeService runtimeService;
	private RepositoryService repositoryService;
	private HistoryService historyService;
	private DynamicBpmnService dynamicBpmnService;
	private FormService formService;
	private IdentityService identityService;
	private ManagementService managementService;
	private ProcessEngineConfiguration processEngineConfiguration;

	@Before
	public void testProcessEngine() {
		processEngine = ProcessEngines.getDefaultProcessEngine();
		System.out.println("流程引擎类：" + processEngine);

		taskService = processEngine.getTaskService();
		runtimeService = processEngine.getRuntimeService();
		repositoryService = processEngine.getRepositoryService();
		historyService = processEngine.getHistoryService();
		dynamicBpmnService = processEngine.getDynamicBpmnService();
		formService = processEngine.getFormService();
		identityService = processEngine.getIdentityService();
		managementService = processEngine.getManagementService();
		processEngineConfiguration = processEngine.getProcessEngineConfiguration();

		String name = processEngine.getName();

		System.out.println("流程引擎的名称： " + name);
		System.out.println(processEngineConfiguration);

	}

	/**
	 * 关闭流程引擎
	 */
	@After
	public void close() {
		processEngine.close();
	}

	/**
	 * 部署独立流程1
	 */
	@Test
	public void deployIndependentprocess1() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("Independentprocess1")
													.name("Independentprocess1")
													.addClasspathResource("独立流程1.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("部署独立流程1,流程ID: " + deploy.getId());
	}

	/**
	 * 部署独立流程2
	 */
	@Test
	public void deployIndependentprocess2() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("Independentprocess2")
													.name("Independentprocess2")
													.addClasspathResource("独立流程2.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("部署独立流程2,流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例
	 * 
	 */
	@Test
	public void startProcessInstanceByKey() {
		String processDefinitionKey = "Independentprocess1";
		runtimeService.startProcessInstanceByKey(processDefinitionKey);
	}

	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "77505";
		Map<String, Object> variables = new HashMap();
		variables.put("sum", 20);
		variables.put("nextDefinitionKey", "Independentprocess2");
		taskService.complete(taskId,variables);
	}
	/**
	 * 完成任务
	 */
	@Test
	public void complete1() {
		String taskId = "85003";
		taskService.complete(taskId);
	}
	
	/**
	 * 触发接受任务完成任务
	 */
	@Test
	public void trigger() {
		runtimeService.trigger("35002");
	}

}
