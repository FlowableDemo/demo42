

------

环境：

[jkd8+]()

[mysql5.6+]()

[实现原理]()：

------

1、在流程1中定义了一个**接受任务**。

2、完成**user1任务**的时候，定义一个变量("nextDefinitionKey")，此变量记录流程2的流程定义key。

3、当执行实例到达**接受任务节点**的时候,接受任务的执行监听器同时将接受任务的执行实例的id以变量形式并传递给流程实例2，此变量为"needTriggerReceiveExecutionId"。

4、同时接受任务的执行监听器获取**流程1**中的变量("nextDefinitionKey")，根据此变量就触发独立**流程2**启动，并传递当前流程的变量给独立**流程2**。

5、全局监听器监听到独立流程2执行完毕后，根据独立流程2中记录的变量(此"needTriggerReceiveExecutionId"变量记录了独立流程1中的接受任务的执行实例的id)，触发接受任务继续执行。此时独立流程2已经结束，流程又回到了独立流程1的**user2任务节点**。

------



## 一、定义两个独立流程

- 独立流程1定义

![](./images/process1.png)

- 独立流程2定义

![](./images/process2.png)



## 二、测试

### 2.1 分别部署两个流程

- 部署流程1

```
运行deployIndependentprocess1()
```

- 部署流程2

```
运行deployIndependentprocess2()
```

- 完成流程1中的user1任务

```java
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "77505";
		Map<String, Object> variables = new HashMap();
		variables.put("sum", 20);
		variables.put("nextDefinitionKey", "Independentprocess2");//流程2的流程定义key
		taskService.complete(taskId,variables);
	}
```

- 查看任务表，出现了两条任务记录:

  一条是流程1的任务user2

  一条是流程2的任务processuer2

- 完成processuer2任务

  完成processuer2任务后，可以看到流程走到了usertask2：因为 sum <30。

```
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "85003";
		taskService.complete(taskId);
	}
```

​	由此可以看到利用服务任务,成功的在流程1中启动了流程2。

- 继续执行完流程2的任务，当流程2中的任务执行完后，流程又回到了流程1上。